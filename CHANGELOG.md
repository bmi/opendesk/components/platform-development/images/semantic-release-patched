# 1.0.0 (2023-12-27)


### Bug Fixes

* **ci:** Move to Open CoDE ([dc3b9fd](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/semantic-release-patched/commit/dc3b9fd28f35b11e9d9e4d700075828f154e074c))
* **dockerfile:** Add pinned versions of semantic-release ([9e7df9d](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/semantic-release-patched/commit/9e7df9dafa7c93d7feec2c10d42fae8abfd636dc))
* **dockerfile:** Use slim image ([4fb36a4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/semantic-release-patched/commit/4fb36a4d2a726047b23a5eafc6ed1b994c9ac155))


### Features

* Initial commit ([67e8761](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/semantic-release-patched/commit/67e87616750430c06b1346176da9acefbd8741b7))
