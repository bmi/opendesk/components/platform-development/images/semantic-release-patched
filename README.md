<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# Semantic Release CI/CD Image (Patched)

This image is created to generate release versions in Gitlab, starting from version **0.0.1**.

## Content:
- [curl](https://github.com/curl/curl)
- [git](https://github.com/git/git)
- [git-lfs](https://github.com/git-lfs/git-lfs)
- [semantic-release](https://github.com/semantic-release/semantic-release)
- [@semantic-release/gitlab](https://github.com/semantic-release/gitlab)
- [@semantic-release/git](https://github.com/semantic-release/git)
- [@semantic-release/release-notes-generator](https://github.com/semantic-release/release-notes-generator)
- [@semantic-release/changelog](https://github.com/semantic-release/changelog)

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright © 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
