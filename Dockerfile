# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0
FROM external-registry.souvap-univention.de/sovereign-workplace/node:18.17.1-bookworm-slim
LABEL maintainer="Univention <info@univention.de>"

RUN apt-get update -qq \
 && apt-get -qq -y install --no-install-recommends \
    git \
    git-lfs \
    curl \
    ca-certificates \
 && apt-get upgrade -y \
 # Install semantic release
 && npm install -g \
    semantic-release@21.1.1 \
    @semantic-release/gitlab@12.0.5 \
    @semantic-release/git@10.0.1 \
    @semantic-release/release-notes-generator@11.0.7 \
    @semantic-release/changelog@6.0.3 \
 && apt-get -y autoremove \
 && apt-get -y clean \
 && rm -rf /var/lib/apt/lists/* \
 && rm -rf /var/cache/apt/archives/* \
 && sed -i "s@1.0.0@0.0.1@g" /usr/local/lib/node_modules/semantic-release/lib/definitions/constants.js
